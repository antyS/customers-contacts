package file.handler;

import file.handler.csv.CsvHandler;
import file.handler.xml.XmlHandler;

public enum SupportedHandlers {
    CSV("csv", new CsvHandler()), XML("xml", new XmlHandler());

    private final String fileType;
    private final Handler handler;

    SupportedHandlers(String fileType, Handler handler) {
        this.fileType = fileType;
        this.handler = handler;
    }

    public static Handler handlerForFileType(String fileType) {
        for (SupportedHandlers sp : SupportedHandlers.values()) {
            if (sp.fileType.equals(fileType)) {
                return sp.handler;
            }
        }
        return null;
    }
}
