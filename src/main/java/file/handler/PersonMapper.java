package file.handler;

import db.model.Contact;
import db.model.Customer;

import java.util.List;

public interface PersonMapper<T extends Person> {

    default Customer toCustomer(T person) {
        Customer customer = new Customer();
        customer.setName(person.getName());
        customer.setSurname(person.getSurname());
        customer.setAge(person.getAge());
        return customer;
    }

    List<Contact> toContacts(T person);
}
