package db.repository;

import org.junit.AfterClass;
import org.junit.Before;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class KnownDbStateTest extends Repository {
    @AfterClass
    public static void afterClass() throws Exception {
        prepareDbState();
    }

    private static void prepareDbState() throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        statement.addBatch("DELETE FROM CONTACTS");
        statement.addBatch("DELETE FROM CUSTOMERS");
        statement.addBatch("ALTER TABLE CUSTOMERS ALTER COLUMN ID RESTART WITH 1");
        statement.addBatch("INSERT INTO CUSTOMERS ( NAME, SURNAME, AGE)VALUES ( 'John', 'Kowalski', 51)");
        statement.addBatch("INSERT INTO CUSTOMERS ( NAME, SURNAME, AGE)VALUES ( 'Kornel', 'Bąk', 31)");
        statement.executeBatch();
    }

    @Before
    public void setUp() throws Exception {
        prepareDbState();
    }

    int getContactCount() throws SQLException {
        return getCount("CONTACTS");
    }

    protected void printDB() throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * from CUSTOMERS right join CONTACTS C ;");
        ResultSetMetaData metaData = resultSet.getMetaData();
        int columnCount = metaData.getColumnCount();

        while (resultSet.next()) {
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                Object object = resultSet.getObject(columnIndex);
                System.out.printf("%s, ", object == null ? "NULL" : object.toString());
            }
            System.out.printf("%n");
        }
    }

    protected int getCustomerCount() throws SQLException {
        return getCount("CUSTOMERS");
    }

    private int getCount(final String table) throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        statement.execute("SELECT COUNT(*) FROM " + table);
        ResultSet resultSet = statement.getResultSet();
        if (resultSet.next()) {
            return resultSet.getInt(1);
        }
        return -1;
    }

}
