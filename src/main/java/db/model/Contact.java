package db.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Contact {
    private Integer id_customer;
    private ContactType type;
    private String contact;

    public Contact(ContactType type, String contact) {
        this.type = type;
        this.contact = contact;
    }
}
