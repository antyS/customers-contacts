import file.handler.Handler;
import file.handler.SupportedHandlers;
import org.apache.commons.cli.CommandLine;

import java.io.IOException;

public class Main {


    public static void main(String[] args) {
        CommandLine cmd = ArgUtil.handleArgs(args);
        if (cmd == null) {
            ArgUtil.printProperUsage();
            return;
        }
        String filePath = cmd.getOptionValue(ArgUtil.FILE_OPT);
        String type = cmd.getOptionValue(ArgUtil.TYPE_OPT);//wykryc po rozszerzeniu ?
        Handler handler = SupportedHandlers.handlerForFileType(type);
        if (handler == null) {
            System.out.println("Wrong file type");
            ArgUtil.printProperUsage();
            return;
        }
        try {
            handler.handle(filePath);
        } catch (IOException e) {
            e.printStackTrace();
            ArgUtil.printProperUsage();
        }
    }
}

