package file.handler.csv;

import db.model.ContactType;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.function.Predicate;

public enum ContactTypeDetector {
    EMAIL(ContactType.EMAIL, contact -> {
        return EmailValidator.getInstance().isValid(contact);
    });

    private final ContactType type;
    private final Predicate<String> predicate;

    ContactTypeDetector(ContactType type, Predicate<String> predicate) {
        this.type = type;
        this.predicate = predicate;
    }

    static public ContactType detect(String c) {
        for (ContactTypeDetector detector : ContactTypeDetector.values()) {
            if (detector.predicate.test(c)) {
                return detector.type;
            }
        }
        return ContactType.UNKNOWN;
    }
}
