package common;


import java.io.IOException;
import java.util.Properties;

public abstract class BaseProperties {
    protected abstract String getPropertiesFilename();

    public Properties getProperties() {
        Properties properties = new Properties();
        try {
            properties.load(BaseProperties.class.getResourceAsStream(getPropertiesFilename()));
            return properties;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
