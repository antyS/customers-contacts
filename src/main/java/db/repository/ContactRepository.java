package db.repository;

import db.model.Contact;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class ContactRepository extends Repository {
    public void save(List<Contact> contacts) {
        Connection connection = getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT  INTO CONTACTS " +
                    "(ID_CUSTOMER, TYPE, CONTACT) VALUES (?,?,?)");
            for (Contact contact : contacts) {
                preparedStatement.setInt(1, contact.getId_customer());
                preparedStatement.setInt(2, contact.getType().ordinal());
                preparedStatement.setString(3, contact.getContact());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
