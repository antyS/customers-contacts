package db.model;

public enum ContactType {
    UNKNOWN, EMAIL, PHONE, JABBER;

    public static ContactType identifyContactType(String contactType) {
        for (ContactType type : ContactType.values()) {
            if (type.name().equalsIgnoreCase(contactType)) {
                return type;
            }
        }
        return UNKNOWN;
    }
}
