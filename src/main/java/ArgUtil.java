import file.handler.SupportedHandlers;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.PrintWriter;
import java.util.Arrays;

class ArgUtil {
    static final String FILE_OPT = "file";
    static final String TYPE_OPT = "type";
    private static final String MAIN_CLASS_NAME = Main.class.getName();

    static CommandLine handleArgs(String[] args) {
        Options options = prepareOptions();
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption(FILE_OPT) && cmd.hasOption(TYPE_OPT)) {
                return cmd;
            } else {
                System.out.println("Wrong arguments");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cmd;
    }

    static void printProperUsage() {
        Options options = prepareOptions();

        HelpFormatter formatter = new HelpFormatter();
        final PrintWriter writer = new PrintWriter(System.out);
        formatter.printUsage(writer, 80, MAIN_CLASS_NAME, options);
        writer.flush();
    }

    private static Options prepareOptions() {
        Options options = new Options();
        Option file = Option.builder()
                .longOpt(FILE_OPT)
                .argName("path")
                .hasArg()
                .desc("Full path to file")
                .required()
                .build();
        Option type = Option.builder()
                .longOpt(TYPE_OPT)
                .argName("name")
                .hasArg()
                .desc("File type. Supported types :"
                        + Arrays.toString(SupportedHandlers.values()))
                .required()
                .build();
        options.addOption(file)
                .addOption(type);
        return options;
    }
}
