package file.handler;

import db.repository.KnownDbStateTest;
import file.handler.csv.CsvHandler;
import file.handler.xml.XmlHandler;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;

public class HandlerTest extends KnownDbStateTest {
    private static final String XML_TEST_FILE = "/test_data/dane-osoby.xml";
    private final XmlHandler xmlHandler = new XmlHandler();
    private final CsvHandler csvHandler = new CsvHandler();

    @Test
    public void handleXml() throws SQLException, IOException {
        //given
        final String filePath = this.getClass().getResource(XML_TEST_FILE).getPath();
        //when
        xmlHandler.handle(filePath);
        //then
        Assert.assertEquals("Wrong customer count", 4, getCustomerCount());
        printDB();
    }

    @Test
    public void handleCsv() throws SQLException, IOException {
        //given
        final String filePath = this.getClass().getResource("/test_data/dane-osoby.csv").getPath();
        //when
        csvHandler.handle(filePath);
        //then
        Assert.assertEquals("Wrong customer count", 4, getCustomerCount());
        printDB();
    }
}
