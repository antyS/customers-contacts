package file.handler;

public interface Person {
    String getName();

    String getSurname();

    Integer getAge();

    String getCity();
}
