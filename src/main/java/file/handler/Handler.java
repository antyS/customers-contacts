package file.handler;

import app.AppProperties;
import db.model.Contact;
import db.model.Customer;
import db.repository.ContactRepository;
import db.repository.CustomerRepository;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;

/**
 * @see SupportedHandlers
 */
public abstract class Handler {

    private final ContactRepository contactRepository = new ContactRepository();
    private final CustomerRepository customerRepository = new CustomerRepository();
    private final AppProperties appProperties = new AppProperties();

    protected abstract void parseAndSave(InputStreamReader file) throws IOException;

    public void handle(String filePath) throws IOException {
        String charsetName = appProperties.getProperties().getProperty(AppProperties.CHARSET);
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(filePath), Charset.forName(charsetName));
        parseAndSave(inputStreamReader);
        inputStreamReader.close();
    }

    protected void save(Customer customer, List<Contact> contacts) {
        final Integer customerId = customerRepository.save(customer);
        contacts.forEach(contact -> contact.setId_customer(customerId));
        contactRepository.save(contacts);
    }

}
