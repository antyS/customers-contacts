package file.handler.csv;

import com.univocity.parsers.annotations.Parsed;
import file.handler.Person;
import lombok.Data;

import java.util.List;

@SuppressWarnings("WeakerAccess")
@Data
public class CsvPerson implements Person {
    @Parsed(index = 0)
    private String name;
    @Parsed(index = 1)
    private String surname;
    @Parsed(index = 2)
    private Integer age;
    @Parsed(index = 3)
    private String city;
    //zakaladam ze nie ma usera z iloscia kontaktow liczona w GB
    static final int CONTACTS_START_COLUMN_INDEX = 4;
    private List<String> contacts;
}
