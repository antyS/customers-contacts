package file.handler.csv;

import db.model.Contact;
import file.handler.PersonMapper;

import java.util.List;
import java.util.stream.Collectors;

public class CsvPersonMapper implements PersonMapper<CsvPerson> {

    @Override
    public List<Contact> toContacts(CsvPerson person) {
        return person.getContacts().stream()
                .map(c -> new Contact(ContactTypeDetector.detect(c), c))
                .collect(Collectors.toList());
    }
}
