package file.handler.csv;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import file.handler.Handler;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class CsvHandler extends Handler {
    private final CsvPersonMapper personMapper = new CsvPersonMapper();

    @Override
    protected void parseAndSave(InputStreamReader file) throws IOException {
        CsvParserSettings settings = new CsvParserSettings();
        CsvPersonConversionProcessor processor = new CsvPersonConversionProcessor();
        settings.setProcessor(processor);
        CsvParser parser = new CsvParser(settings);
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            parser.parseLine(line);//troche over engineering
            CsvPerson person = processor.getLastProcessed();// srednio to wyglada
            save(personMapper.toCustomer(person), personMapper.toContacts(person));
        }
        if (sc.ioException() != null) {
            throw sc.ioException();
        }
        sc.close();
    }

}
