package file.handler.xml;

import file.handler.Handler;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStreamReader;

public class XmlHandler extends Handler {

    private static final String PERSON = "person";
    private final XmlPersonMapper xmlPersonMapper = new XmlPersonMapper();

    @Override
    protected void parseAndSave(InputStreamReader fileStream) {
        try {
            XMLInputFactory inputFactory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = inputFactory.createXMLEventReader(fileStream);
            JAXBContext jc = JAXBContext.newInstance(XmlPerson.class);
            Unmarshaller unmarshaller = jc.createUnmarshaller();

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.peek();
                if (event.isStartElement()) {
                    String startElementName = event.asStartElement().getName().getLocalPart();
                    if (PERSON.equals(startElementName)) {
                        final XmlPerson person = (XmlPerson) unmarshaller.unmarshal(eventReader);
                        save(xmlPersonMapper.toCustomer(person), xmlPersonMapper.toContacts(person));
                    }
                }
                if (eventReader.hasNext()) {
                    eventReader.nextEvent();
                }
            }
        } catch (JAXBException | XMLStreamException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
