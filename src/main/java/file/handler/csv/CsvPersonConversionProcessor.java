package file.handler.csv;

import com.univocity.parsers.annotations.helpers.MethodFilter;
import com.univocity.parsers.common.ParsingContext;
import com.univocity.parsers.common.processor.core.BeanConversionProcessor;
import com.univocity.parsers.common.processor.core.Processor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//https://github.com/uniVocity/univocity-parsers/issues/269
public class CsvPersonConversionProcessor extends BeanConversionProcessor<CsvPerson> implements Processor<ParsingContext> {
    @Getter
    private CsvPerson lastProcessed;

    CsvPersonConversionProcessor() {
        super(CsvPerson.class, MethodFilter.ONLY_SETTERS);
    }

    @Override
    public void processStarted(ParsingContext context) {
        super.initialize(context.headers());
    }

    @Override
    public void rowProcessed(String[] row, ParsingContext context) {
        CsvPerson person = createBean(row, context);
        if (person != null) {
            List<String> contacts = new ArrayList<>(
                    Arrays.asList(row)
                            .subList(CsvPerson.CONTACTS_START_COLUMN_INDEX, row.length));
            person.setContacts(contacts);
            this.lastProcessed = person;
        }
    }

    @Override
    public void processEnded(ParsingContext context) {
    }
}
