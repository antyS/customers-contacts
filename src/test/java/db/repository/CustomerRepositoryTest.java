package db.repository;

import db.model.Customer;
import org.junit.Assert;
import org.junit.Test;

public class CustomerRepositoryTest extends KnownDbStateTest {

    private final CustomerRepository repository = new CustomerRepository();

    @Test
    public void save() {
        //given
        Customer customer = new Customer("Albert", "Jamal", 123);
        //when
        int id = repository.save(customer);
        //then
        Assert.assertEquals("Wrong customer id", 3, id);
    }
}
