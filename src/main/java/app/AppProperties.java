package app;

import common.BaseProperties;

public class AppProperties extends BaseProperties {

    public static final String CHARSET = "charset";

    private static final String APP_PROPERTIES = "/app.properties";

    @Override
    protected String getPropertiesFilename() {
        return APP_PROPERTIES;
    }
}
