package db.repository;

import db.model.Contact;
import db.model.ContactType;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ContactRepositoryTest extends KnownDbStateTest {
    private final ContactRepository repository = new ContactRepository();

    @Test
    public void save() throws SQLException {
        //given
        List<Contact> list = new ArrayList<>();
        list.add(new Contact(1, ContactType.EMAIL, "john.kowalski@gmail.com"));
        list.add(new Contact(1, ContactType.EMAIL, "kornel.bak@gmail.com"));
        //when
        repository.save(list);
        //then
        Assert.assertEquals("Wrong contact count", list.size(), getContactCount());
    }

}
