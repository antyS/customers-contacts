package file.handler.xml;

import file.handler.Person;
import lombok.Data;
import org.w3c.dom.Element;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "person")
@Data
@XmlAccessorType(XmlAccessType.FIELD)
class XmlPerson implements Person {

    @XmlElement
    private String name;
    @XmlElement
    private String surname;
    @XmlElement
    private Integer age;
    @XmlElement
    private String city;
    @XmlElementWrapper
    @XmlAnyElement
    private List<Element> contacts;

}
