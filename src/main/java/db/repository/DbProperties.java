package db.repository;

import common.BaseProperties;

class DbProperties extends BaseProperties {

    static final String DRIVER = "driver";
    static final String USER = "user";
    static final String PASSWORD = "password";
    static final String URL = "url";

    private static final String DATABASE_PROPERTIES = "/database.properties";

    @Override
    protected String getPropertiesFilename() {
        return DATABASE_PROPERTIES;
    }
}
