package file.handler.xml;

import db.model.Contact;
import db.model.ContactType;
import file.handler.PersonMapper;

import java.util.List;
import java.util.stream.Collectors;

class XmlPersonMapper implements PersonMapper<XmlPerson> {

    @Override
    public List<Contact> toContacts(XmlPerson person) {
        return person.getContacts().stream().map(element -> new Contact(
                ContactType.identifyContactType(element.getTagName()),
                element.getTextContent())).collect(Collectors.toList());
    }
}
