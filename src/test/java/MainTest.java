import db.repository.KnownDbStateTest;
import org.junit.Assert;
import org.junit.Test;

import java.sql.SQLException;

public class MainTest extends KnownDbStateTest {

    @Test
    public void testPassingXml() throws SQLException {
        //given
        final String[] args = {
                "--file",
                this.getClass().getResource("/test_data/dane-osoby.xml").getPath(),
                "--type","xml"
        };
        //when
        Main.main(args);
        //then
        Assert.assertEquals("Wrong customer count", 4, getCustomerCount());
        printDB();
    }
    @Test
    public void testPassingCsv() throws SQLException {
        //given
        final String[] args = {
                "--file",
                this.getClass().getResource("/test_data/dane-osoby.csv").getPath(),
                "--type","csv"
        };
        //when
        Main.main(args);
        //then
        Assert.assertEquals("Wrong customer count", 4, getCustomerCount());
        printDB();
    }
}
