package db.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

abstract class Repository {

    private static final DbProperties dbProperties = new DbProperties();

    static Connection getConnection() {
        Connection connection;
        Properties properties = dbProperties.getProperties();
        try {

            Class.forName(properties.getProperty(DbProperties.DRIVER));
            connection = DriverManager.getConnection(
                    properties.getProperty(DbProperties.URL),
                    properties.getProperty(DbProperties.USER),
                    properties.getProperty(DbProperties.PASSWORD));
            System.out.println("Connection OK");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            System.out.println("Connection ERROR");
            throw new RuntimeException(e);
        }
        return connection;
    }

}
